Introdução
==========
Meu jogo legal! **NOME DO JOGO**, um jogo estilo **GÊNERO** com
**CARACTERÍSTICAS BACANAS**. Jogue **MULTIPLAYER/JOGADOR SOLO**.

> Nesta seção, você deve colocar os elementos básicos do seu jogo. Se possível,
também tente convencer o leitor de que seu jogo é um bom investimento, falando
de como a jogabilidade é bacana e com algumas frases de efeito. É válido
mencionar tudo o mais que está descrito aqui, de maneira concisa e empolgante.

Descrição do jogo
================

> Plataforma, gênero, mecânica, público alvo, gameplay e experiência de
gameplay, _concept art_ ilustrando os conceitos a serem seguidos, desde à arte
à programação do jogo.

Narração do jogo
================
No reino de **NOME DE REINO GENÉRICO**, o Grande Mal derramou suas asas sobre 
o reino, e apenas o Herói **NOME DO HERÓI MARCANTE** é capaz de salvar o mundo!

> Esta é uma das seções de descrições do jogo. Você deve passar as informações
diegéticas do jogo. Pode entrar em detalhes sobre o economia e sociedade, se
essas análises forem relevantes ao jogo. Todos os detalhes de enredo se
encaixam aqui.

> Não se esqueça de determinar um arco de crescimento do personagem baseado
na Jornada do Herói! Ou fuja dela de modo consciente.

Ambientação/Concept art
=======================

Experiência estética
---------------------
> Ok, sem texto genérico para esta parte. Descreva aqui a pegada do jogo, como
vai ser a estética do jogo. Tanto o esquema de cores como as experiências
desejáveis a se passar ao jogador. Desde a construção do personagem, como
roupas, até ambientação externa como hora do dia, jogo de sombras,
iluminação.

Ambiente sonoro
---------------
> Uma experiência sonora que entre em conformidade com a experiência estética
e com o enredo/narração fornece uma experiência mais autêntica.

Jogabilidade
============

> O jogador vai interagir como com o mundo do jogo? Controle, detector de
movimento, teclado, voz? Isso se encaixa no gênero de jogo desejado? Quais
são as experiências que o jogador terá?

> Vai ser de um jogador? Ou de múltiplos jogadores? Os jogadores seguirão
papéis distintos? Como vai ser o controle que o jogador terá sobre o
jogador?

> Regras e power ups.

Jogadores
=========
> Público alvo desejado. O que cada pessoa do público alvo terá de volta do
jogo.

Level design
============
> Os elementos que compõe o cenário, a disposição de personagens não jogadores,
seus papéis, ambientação, evolução temporal que o cenário sofre. Tesouros,
itens colecionáveis, itens que mudam as mecânicas são importantes e relevantes
para ampliar a experiência de imersão no enredo.

> Música, som, luz e outras experiências estéticas em um level pode e deve ser
distintas da experiência geral do jogo, caracterizando o level individualmente.
Ambientação bem feita proporcional uma experiência mais fidedigna.

Sequência de telas
==================

> Organograma da sequência de telas. Desde o menu inicial, definição de
ajsutes. Coloque uma imagem com esse organograma para ficar mais fácil a
leitura.

Prototipagem
=============

> Prototipagem serve para testar mecânicas, regras, interface com o jogador
para saber se o objeto do game design está sendo seguido. Também se identifica
em quais plataformas é possível rodar (capacidade de hardware e de
jogabilidade). Não há necessidade de refinamento estético, visual nem de enredo
nesta parte.

Considerações finais
====================
Vamos tomar cerveja! Nosso jogo é muito fodão!!!

> O jogo atende aos enseios previstos? A jogabilidade é viável? Ficou fluída?